import axios from "axios";

const xantiayadakAPI = axios.create({
  baseURL: "http://185.208.172.195:8080/api",
  // baseURL: "http://localhost:8080/api",
});

xantiayadakAPI.interceptors.request.use(
  async (config) => {
    const newConfig = config;
    const token = typeof window !== 'undefined' && localStorage.getItem('auth-token');
    if (token) {
      newConfig.headers['x-auth-token'] = token;
    }
    newConfig.headers['Content-Type'] = 'application/json';
    return newConfig;
  },
);

export default xantiayadakAPI;