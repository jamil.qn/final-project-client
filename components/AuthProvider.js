import React, { useEffect, useState } from "react";
import { useRouter } from "next/dist/client/router";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
}));

const AuthProvider = ({ children }) => {
  const classes = useStyles();
  const router = useRouter();

  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const token = localStorage.getItem("auth-token");
    if (!token) {
      router.push("/admin/signin");
    } else {
      setTimeout(() => {
        setIsLoading(false);
      }, 1000);
    }
  }, [router]);

  return (
    <>
      {isLoading ? (
        <Backdrop
          className={classes.backdrop}
          open={isLoading}
          style={{ flexDirection: "column" }}
        >
          <CircularProgress color="inherit" />
          <Typography variant="body2">لطفا کمی منتظر بمانید...</Typography>
        </Backdrop>
      ) : (
        <div>{children}</div>
      )}
    </>
  );
};
export default AuthProvider;
