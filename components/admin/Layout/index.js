import React from "react";
import Link from "next/link";
import { useRouter } from "next/dist/client/router";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import {
  Drawer,
  AppBar,
  Toolbar,
  List,
  CssBaseline,
  Typography,
  Divider,
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemText,
  Tooltip,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import CategoryIcon from "@material-ui/icons/Category";
import LocalMallIcon from "@material-ui/icons/LocalMall";
import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginRight: drawerWidth,
  },
  menuButton: {
    marginLeft: 36,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(1),
    paddingRight: theme.spacing(31),
    [theme.breakpoints.only('xs')]: {
      paddingRight: theme.spacing(8),
    }
  },
}));

export default function Layout({ children }) {
  const router = useRouter();
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const logoutHandler = () => {
    localStorage.removeItem("auth-token");
    router.push("/");
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <Link href="/admin">
            <a>
              <Typography variant="h6" noWrap>
                پنل ادمین
              </Typography>
            </a>
          </Link>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        anchor="right"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <List>
          <Link href="/admin/product-categories/product-categories-list">
            <a>
              <ListItem button>
                <Tooltip title="دسته بندی محصولات" placement="left">
                  <ListItemIcon>
                    <CategoryIcon />
                  </ListItemIcon>
                </Tooltip>
                <ListItemText primary={"دسته بندی محصولات"} />
              </ListItem>
            </a>
          </Link>
          <Divider variant="middle" component="li" />
          <Link href="/admin/products/products-list">
            <a>
              <ListItem button>
                <Tooltip title="لیست محصولات" placement="left">
                  <ListItemIcon>
                    <LocalMallIcon />
                  </ListItemIcon>
                </Tooltip>
                <ListItemText primary={"لیست محصولات"} />
              </ListItem>
            </a>
          </Link>
          <Divider variant="middle" component="li" />
          <ListItem button onClick={logoutHandler}>
            <Tooltip title="خروج" placement="left">
              <ListItemIcon>
                <PowerSettingsNewIcon />
              </ListItemIcon>
            </Tooltip>
            <ListItemText primary={"خروج"} />
          </ListItem>
        </List>
      </Drawer>
      <main
        className={classes.content}
        style={!open ? { paddingRight: "64px" } : {}}
      >
        <div className={classes.toolbar} />
        {children}
      </main>
    </div>
  );
}
