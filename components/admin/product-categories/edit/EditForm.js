import React, { useState, useEffect } from "react";
import { useRouter } from "next/dist/client/router";
import {
  TextField,
  FormControl,
  InputLabel,
  NativeSelect,
  Button,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import xantiayadakAPI from "../../../../apis/xantiayadak";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: 200,
    },
  },
  inputGroup: {
    display: "flex",
    marginBottom: theme.spacing(2),
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
    },
  },
  formControl: {
    margin: theme.spacing(1),
    width: 200,
  },
}));

export default function EditForm({ data }) {
  const router = useRouter();
  const classes = useStyles();

  const [categories, setCategories] = useState([]);
  const [parentId, setParentId] = useState(data.parent_id);
  const [categoryName, setCategoryName] = useState(data.name);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    getProductCategories();
  }, []);

  const getProductCategories = async () => {
    const productCategories = await xantiayadakAPI.get("/product-categories");
    setCategories(productCategories.data.data);
  };

  const selectOptionsRender = categories.map((category) => {
    if (!category.parent_id) {
      return (
        <option
          key={category._id}
          value={category._id}
          selected={category._id === data.parent_id ? true : false}
        >
          {category.name}
        </option>
      );
    }
  });

  const onFormSubmit = async (event) => {
    event.preventDefault();
    try {
      const res = await xantiayadakAPI.put(`/product-categories/${data._id}`, {
        name: categoryName,
        parent_id: parentId ? parentId : null,
      });
      if (res.status === 200) {
        router.push("/admin/product-categories/product-categories-list");
      }
    } catch (err) {
      if (err.response.status === 401 || err.response.status === 403) {
        setErrorMessage(
          err.response.data.message + " انتقال به صفحه احراز هویت..."
        );
        setTimeout(() => {
          router.push("/admin/signin");
        }, 1000);
      } else {
        setErrorMessage(err.response.data.message);
      }
    }
  };

  return (
    <form className={classes.root} onSubmit={onFormSubmit}>
      <Typography color="secondary">{errorMessage}</Typography>
      <div className={classes.inputGroup}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="parent">والد</InputLabel>
          <NativeSelect
            dir="ltr"
            value={parentId}
            onChange={(e) => setParentId(e.target.value)}
            inputProps={{
              id: "parent",
            }}
          >
            <option value="">خودش والد است</option>
            {selectOptionsRender}
          </NativeSelect>
        </FormControl>
        <TextField
          value={categoryName}
          onChange={(e) => setCategoryName(e.target.value)}
          label="نام دسته"
          required
        />
      </div>
      <Button
        variant="contained"
        color="primary"
        type="submit"
        onSubmit={onFormSubmit}
      >
        ذخیره
      </Button>
    </form>
  );
}
