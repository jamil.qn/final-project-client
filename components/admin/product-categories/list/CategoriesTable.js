import React, { useState } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableRowBuilder from "./TableRowBuilder";
import xantiayadakAPI from "../../../../apis/xantiayadak";

const columns = [
  { id: "code", label: "ردیف" },
  { id: "name", label: "نام" },
  {
    id: "mainCategory",
    label: "دسته اصلی",
  },
  {
    id: "actions",
    label: "عملیات",
  },
];

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: "100px",
  },
  tablePagination: {
    "& .MuiToolbar-root": {
      display: "flex",
      flexDirection: "row-reverse",
      justifyContent: "flex-end",
      "& .MuiTablePagination-actions": {
        display: "flex",
        flexDirection: "row-reverse",
      },
    },
  },
}));

export default function CategoriesTable({ categoriesData }) {
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [categories, setCategories] = useState(categoriesData);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleAfterDeleteCatgory = async () => {
    const res = await xantiayadakAPI.get("/product-categories");
    setCategories(res.data.data);
  };

  const tableBodyRender = categories
    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
    .map((category, index) => {
      return (
        <TableRowBuilder
          key={category._id}
          category={category}
          rowNumber={index + 1 + page * rowsPerPage}
          handleAfterDeleteCatgory={handleAfterDeleteCatgory}
        />
      );
    });

  return (
    <>
      <TableContainer>
        <Table className={classes.table} aria-label="categories table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <StyledTableCell align="right" key={column.id}>
                  {column.label}
                </StyledTableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>{tableBodyRender}</TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        className={classes.tablePagination}
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={categories.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        labelRowsPerPage=": در هر صفحه"
      />
    </>
  );
}
