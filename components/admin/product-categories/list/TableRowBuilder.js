import { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/dist/client/router";
import {
  TableRow,
  TableCell,
  IconButton,
  CircularProgress,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import xantiayadakAPI from "../../../../apis/xantiayadak";

const useStyles = makeStyles((theme) => ({
  actions: {
    display: "flex",
  },
  fabProgress: {
    position: "absolute",
    zIndex: 1,
  },
}));

const TableRowBuilder = ({ category, rowNumber,handleAfterDeleteCatgory }) => {
  const classes = useStyles();
  const router = useRouter();

  const [loading, setLoading] = useState(false);
  const [parentName, setParentName] = useState("#");
  useEffect(() => {
    findParentName();
  }, []);

  const findParentName = async () => {
    try {
      if (category.parent_id) {
        const mainCategory = await xantiayadakAPI.get(
          `/product-categories/${category.parent_id}`
        );
        setParentName(mainCategory.data.data.name);
      }
    } catch (err) {
      console.log(err.message);
    }
  };

  const deleteHandler = async () => {
    try {
      const confirmText = category.parent_id
        ? `آیا با پاک کرد دسته *${category.name}* موافقی چرا که با پاک کردنش تمام محصولات زیر مجموعه اش هم پاک خواهند شد؟!`
        : `آیا با پاک کرد دسته *${category.name}* موافقی چرا که با پاک کردنش تمام زیر دسته ها به همراه محصولاتشان پاک خواهند شد؟!`;
      if (confirm(confirmText)) {
        setLoading(true);
        const resultDelete = await xantiayadakAPI.delete(
          `/product-categories/${category._id}`
        );
        if (resultDelete.status === 200) {
          setLoading(false);
          handleAfterDeleteCatgory();
        }
      } else {
        return;
      }
    } catch (err) {
      if (err.response.status === 401 || err.response.status === 403) {
        setTimeout(() => {
          router.push("/admin/signin");
        }, 1000);
      }
      setLoading(false);
    }
  };

  return (
    <TableRow hover role="checkbox" tabIndex={-1}>
      <TableCell align="right">{rowNumber}</TableCell>
      <TableCell align="right">{category.name}</TableCell>
      <TableCell align="right">{parentName}</TableCell>
      <TableCell align="right">
        <div className={classes.actions}>
          <Link
            href={`/admin/product-categories/edit-category/${category._id}`}
          >
            <a>
              <IconButton aria-label="edit">
                <EditIcon />
              </IconButton>
            </a>
          </Link>
          <IconButton
            onClick={deleteHandler}
            aria-label="delete"
            color="secondary"
            className={classes.deleteButton}
            disabled={loading ? true : false}
          >
            <DeleteIcon />
            {loading && (
              <CircularProgress
                size={35}
                color="secondary"
                className={classes.fabProgress}
              />
            )}
          </IconButton>
        </div>
      </TableCell>
    </TableRow>
  );
};

export default TableRowBuilder;
