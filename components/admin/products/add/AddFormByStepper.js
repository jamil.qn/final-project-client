import React, { useState, useEffect } from "react";
import { useRouter } from "next/dist/client/router";
import {
  FormControl,
  NativeSelect,
  InputLabel,
  TextField,
  Stepper,
  Step,
  StepLabel,
  Button,
  Typography,
  CircularProgress,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
const ReactQuill =
  typeof window === "object" ? require("react-quill") : () => false;
import { DropzoneArea } from "material-ui-dropzone";
import xantiayadakAPI from "../../../../apis/xantiayadak";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& .MuiStepper-root": {
      overflowX: 'scroll',
      borderBottom: `3px solid ${theme.palette.primary.main}`,
    },
  },
  backButton: {
    marginLeft: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(5),
  },
  inputGroup: {
    display: "flex",
    marginBottom: theme.spacing(2),
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
      '& $input': {
        width: '100%',
      },
    },
  },
  input: {
    width: '300px',
    marginLeft: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  priceInput: {
    "& .MuiInputBase-root": {
      "& .MuiInputBase-input": {
        direction: "ltr",
        "&[type=number]": {
          "-moz-appearance": "textfield",
        },
        "&::-webkit-outer-spin-button": {
          "-webkit-appearance": "none",
          margin: 0,
        },
        "&::-webkit-inner-spin-button": {
          "-webkit-appearance": "none",
          margin: 0,
        },
      },
    },
  },
  textEditor: {
    marginBottom: theme.spacing(3),
    "& .quill": {
      direction: "ltr",
      "& .ql-container": {
        "& .ql-editor": {
          textAlign: "right",
          direction: "rtl",
          height: "200px",
        },
      },
    },
  },
  dropZoneArea: {
    "& .MuiDropzoneArea-root": {
      padding: 50,
    },
  },
}));

function getSteps() {
  return ["اطلاعات پایه", "اطلاعات فنی", "مشخصات فنی", "اطلاعات نهایی"];
}

export default function AddFormByStepper() {
  const router = useRouter();
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();
  const [name, setName] = useState("");
  const [productCategoryId, setProductCategoryId] = useState("");
  const [price, setPrice] = useState();
  const [discount, setDiscount] = useState(0);
  const [inventory, setInventory] = useState();
  const [description, setDescription] = useState("");
  const [specification, setSpecification] = useState("");
  const [pictures, SetPictures] = useState([]);
  const [isInRequest, setIsInRequest] = useState(false);
  const [categories, setCategories] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    getProductCategories();
  }, []);

  const getProductCategories = async () => {
    const productCategories = await xantiayadakAPI.get("/product-categories");
    setCategories(productCategories.data.data);
  };

  const modules = {
    toolbar: [
      [{ header: [1, 2, 3, 4, 5, 6, false] }],
      ["bold", "italic", "underline", "strike", "blockquote"],
      [
        { list: "ordered" },
        { list: "bullet" },
        { indent: "-1" },
        { indent: "+1" },
      ],
      ["link", "image"],
      [{ direction: "rtl" }],
    ],
  };

  const formats = [
    "header",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
  ];

  const handleNext = () => {
    activeStep === steps.length
      ? null
      : setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setErrorMessage("");
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleAddRequest = async () => {
    if (
      name &&
      productCategoryId &&
      price &&
      inventory &&
      description &&
      specification &&
      pictures
    ) {
      setIsInRequest(true);
      const productData = new FormData();
      productData.append("product_category_id", productCategoryId);
      productData.append("name", name);
      productData.append("price", price);
      productData.append("discount", discount);
      productData.append("inventory", inventory);
      productData.append("description", description);
      productData.append("specification", specification);
      pictures.map((picture) => productData.append("pictures", picture));
      try {
        const res = await xantiayadakAPI.post("/products", productData, {
          headers: {
            "Content-Type": `multipart/form-data; boundary=${productData._boundary}`,
          },
        });
        if (res.status === 200) {
          router.push("/admin/products/products-list");
        } else {
          console.log(res);
          setIsInRequest(false);
        }
      } catch (err) {
        if (err.response.status === 401 || err.response.status === 403) {
          setErrorMessage(
            err.response.data.message + " انتقال به صفحه احراز هویت..."
          );
          setTimeout(() => {
            router.push("/admin/signin");
          },1000)
        } else {
          setErrorMessage(err.response.data.message);
        }
        setIsInRequest(false);
      }
    } else {
      setErrorMessage("یکی از فیلدها پر نشده است");
    }
  };

  const selectOptionsRender = categories.map((category) => {
    if (category.parent_id) {
      return (
        <option key={category._id} value={category._id}>
          {category.name}
        </option>
      );
    }
  });

  function getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <div className={classes.inputGroup}>
            <TextField
              className={classes.input}
              value={name}
              onChange={(e) => setName(e.target.value)}
              label="نام محصول"
              required
            />
            <FormControl className={classes.input}>
              <InputLabel htmlFor="parent">دسته بندی</InputLabel>
              <NativeSelect
                dir="ltr"
                value={productCategoryId}
                onChange={(e) => setProductCategoryId(e.target.value)}
                inputProps={{
                  id: "parent",
                }}
              >
                <option value=""></option>
                {selectOptionsRender}
              </NativeSelect>
            </FormControl>
          </div>
        );
      case 1:
        return (
          <div className={classes.inputGroup}>
            <TextField
              type="number"
              className={`${classes.input} ${classes.priceInput}`}
              value={price}
              onChange={(e) => setPrice(e.target.value)}
              label="قیمت به تومان"
              required
            />
            <TextField
              type="number"
              className={classes.input}
              value={discount}
              onChange={(e) => setDiscount(e.target.value)}
              label="تخفیف"
              required
            />
            <TextField
              type="number"
              className={classes.input}
              value={inventory}
              onChange={(e) => setInventory(e.target.value)}
              label="موجودی"
              required
            />
          </div>
        );
      case 2:
        return (
          <>
            <div className={classes.textEditor}>
              <Typography variant="h5" style={{ marginBottom: "16px" }}>
                توضیحاتی درباره محصول {name}...
              </Typography>
              <ReactQuill
                theme="snow"
                value={description}
                onChange={(content, delta, source, editor) =>
                  setDescription(content)
                }
                modules={modules}
                formats={formats}
                placeholder={`توضیحاتی درباره محصول ${name} را بنویسید...`}
              />
            </div>
            <div className={classes.textEditor}>
              <Typography variant="h5" style={{ marginBottom: "16px" }}>
                ویژگی محصول {name}...
              </Typography>
              <ReactQuill
                theme="snow"
                value={specification}
                onChange={(content, delta, source, editor) =>
                  setSpecification(content)
                }
                modules={modules}
                formats={formats}
                placeholder={`ویژگی محصول ${name} را بنویسید...`}
              />
            </div>
          </>
        );
      case 3:
        return (
          <div className={classes.dropZoneArea}>
            <DropzoneArea
              filesLimit={10}
              maxFileSize={250000}
              initialFiles={pictures}
              acceptedFiles={["image/jpeg", "image/png", "image/jpg"]}
              dropzoneText="با کشیدن و رها کردن عکس در این محل میتوانید عکس های محصول را آپلود کنید. تنها به یاد داشته باشید اولین عکس مربوط به عکس اصلی محصول میباشد.:))"
              onChange={(files) => SetPictures(files)}
            />
          </div>
        );
      default:
        return "Unknown stepIndex";
    }
  }

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div>
            <div className={classes.instructions}>
              <Typography>
                اگر از درست بودن اطلاعات وارد شده مطمئن هستید <b>دکمه ذخیره</b>{" "}
                محصول را بزنید
              </Typography>
              <Typography color="secondary">{errorMessage}</Typography>
            </div>
            {isInRequest ? (
              <CircularProgress />
            ) : (
              <>
                <Button
                  disabled={activeStep === 0}
                  onClick={handleBack}
                  className={classes.backButton}
                >
                  قبلی
                </Button>
                <Button
                  disabled={errorMessage}
                  variant="contained"
                  color="primary"
                  onClick={handleAddRequest}
                >
                  ذخیره محصول
                </Button>
              </>
            )}
          </div>
        ) : (
          <div>
            <div className={classes.instructions}>
              {getStepContent(activeStep)}
            </div>
            <div>
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.backButton}
              >
                قبلی
              </Button>
              <Button variant="contained" color="primary" onClick={handleNext}>
                بعدی
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
