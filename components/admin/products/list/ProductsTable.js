import React, { useState } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableRowBuilder from "./TableRowBuilder";
import xantiayadakAPI from "../../../../apis/xantiayadak";

const columns = [
  { id: "code", label: "ردیف" },
  { id: "image", label: "‫عکس‬ ‫محصول‬" },
  { id: "name", label: "نام محصول" },
  {
    id: "price",
    label: "قیمت",
  },
  {
    id: "discount",
    label: "تخفیف",
  },
  {
    id: "actions",
    label: "عملیات",
  },
];

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const useStyles = makeStyles((theme) => ({
  tablePagination: {
    "& .MuiToolbar-root": {
      display: "flex",
      flexDirection: "row-reverse",
      justifyContent: 'flex-end',
      "& .MuiTablePagination-actions": {
        display: "flex",
        flexDirection: "row-reverse",
      },
    },
  },
}));

export default function ProductsTable({ productsData }) {
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [products, setProducts] = useState(productsData);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const handleAfterDeleteCatgory = async () => {
    const res = await xantiayadakAPI.get("/products");
    setProducts(res.data.data);
  };

  const tableBodyRender = products
    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
    .map((product, index) => {
      return (
        <TableRowBuilder
          key={product._id}
          product={product}
          rowNumber={(index + 1) + (page * rowsPerPage)}
          handleAfterDeleteCatgory={handleAfterDeleteCatgory}
        />
      );
    });

  return (
    <>
      <TableContainer>
        <Table aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <StyledTableCell align="right" key={column.id}>
                  {column.label}
                </StyledTableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>{tableBodyRender}</TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        className={classes.tablePagination}
        rowsPerPageOptions={[5, 10, 15, 25, 100]}
        component="div"
        count={products.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        labelRowsPerPage={": در هر صفحه"}
      />
    </>
  );
}
