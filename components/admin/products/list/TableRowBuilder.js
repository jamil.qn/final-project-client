import { useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/dist/client/router";
import {
  TableRow,
  TableCell,
  IconButton,
  CircularProgress,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import xantiayadakAPI from "../../../../apis/xantiayadak";
import { format } from "../../../../utils";

const useStyles = makeStyles((theme) => ({
  actions: {
    display: "flex",
  },
  imageBox: {
    width: 180,
    height: 150,
    position: "relative",
  },
  fabProgress: {
    position: "absolute",
    zIndex: 1,
  },
}));

const TableRowBuilder = ({ product, rowNumber,handleAfterDeleteCatgory }) => {
  const classes = useStyles();
  const router = useRouter();

  const [loading, setLoading] = useState(false);

  const deleteHandler = async () => {
    try {
      const confirmText =  `آیا با پاک کردن محصول *${product.name}* موافقی؟!`;
      if (confirm(confirmText)) {
        setLoading(true);
        const resultDelete = await xantiayadakAPI.delete(
          `/products/${product._id}`
        );
        if (resultDelete.status === 200) {
          setLoading(false);
          handleAfterDeleteCatgory();
        } else {
          console.log(resultDelete);
        }
      } else {
        return;
      }
    } catch (err) {
      if (err.response.status === 401 || err.response.status === 403) {
        setTimeout(() => {
          router.push("/admin/signin");
        }, 1000);
      }
      setLoading(false);
    }
  };

  return (
    <TableRow hover role="checkbox" tabIndex={-1}>
      <TableCell align="right">{rowNumber}</TableCell>
      <TableCell align="right">
        <div className={classes.imageBox}>
          <Image
            src={product.pictures[0].path}
            alt={product.pictures[0].alt}
            layout="fill"
          />
        </div>
      </TableCell>
      <TableCell align="right">{product.name}</TableCell>
      <TableCell align="right">{format(product.price)} تومان</TableCell>
      <TableCell align="right">{format(product.discount)}٪</TableCell>
      <TableCell align="right">
        <div className={classes.actions}>
          <Link
            href={`/admin/products/edit-product/${product._id}`}
          >
            <a>
              <IconButton aria-label="edit">
                <EditIcon />
              </IconButton>
            </a>
          </Link>
          <IconButton
            onClick={deleteHandler}
            aria-label="delete"
            color="secondary"
            className={classes.deleteButton}
            disabled={loading ? true : false}
          >
            <DeleteIcon />
            {loading && (
              <CircularProgress
                size={35}
                color="secondary"
                className={classes.fabProgress}
              />
            )}
          </IconButton>
        </div>
      </TableCell>
    </TableRow>
  );
};

export default TableRowBuilder;
