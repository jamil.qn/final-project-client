import { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import { TableCell, TableRow, IconButton } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { format, calculateTheFinalPrice } from "../../../utils";

import QuantityCounter from "../../global/buttons/QuantityCounter";

const TableRowBuilder = ({ product, setquantity, removeHandler, totalPriceHandler }) => {
  const [quantity, setQuantity] = useState(setquantity);

  useEffect(() => {
    updateProductInLocaleStorage();
  }, [quantity]);

  const updateProductInLocaleStorage = () => {
    const products = localStorage.getItem("products")
      ? JSON.parse(localStorage.getItem("products"))
      : {};

    products[product._id].setquantity = quantity;

    localStorage.setItem("products", JSON.stringify(products));
  };

  const quantityHandler = (count) => {
    setQuantity(count);
    totalPriceHandler();
  };

  const finalPrice = calculateTheFinalPrice(
    product.price,
    product.discount,
    quantity
  );

  return (
    <TableRow hover role="checkbox" tabIndex={-1} key={product._id}>
      <TableCell align="right">
        <Link href={`product-detail/${product._id}`}>
          <a>
            <div style={{ width: "180px", height: "150px", position: 'relative' }}>
              <Image
                src={product.pictures[0].path}
                alt={product.pictures[0].alt}
                layout="fill"
              />
            </div>
          </a>
        </Link>
      </TableCell>
      <TableCell style={{ minWidth: 170 }} align="right">
        {product.name}
      </TableCell>
      <TableCell align="right">{format(product.price)} تومان</TableCell>
      <TableCell align="right">{format(product.discount)}٪</TableCell>
      <TableCell align="right">
        <QuantityCounter
          inventory={product.inventory}
          quantity={quantity}
          quantityHandler={quantityHandler}
        />
      </TableCell>
      <TableCell align="right">{format(finalPrice)} تومان</TableCell>
      <TableCell align="right">
        <IconButton
          aria-label="delete"
          color="secondary"
          onClick={()=> removeHandler(product._id)}
        >
          <DeleteIcon />
        </IconButton>
      </TableCell>
    </TableRow>
  );
};

export default TableRowBuilder;
