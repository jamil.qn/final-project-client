import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import TableRowBuilder from "./TableRowBuilder";
import { calculateTheFinalPrice, format } from "../../../utils";
import { useEffect, useState } from "react";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: "600px",
  },
  table: {
    minWidth: "650px",
  },
  total_price_container: {
    borderTop: "1px solid  rgba(0, 0, 0, 0.12)",
    padding: "20px",
  },
}));

const CartTable = () => {
  const classes = useStyles();
  const [totalPrice, setTotalPrice] = useState(0);

  const [cartProducts, setCartProducts] = useState({});

  useEffect(() => {
    getCartProducts();
  }, []);

  const getCartProducts = () => {
    setCartProducts(() =>
      localStorage.getItem("products")
        ? JSON.parse(localStorage.getItem("products"))
        : {}
    );
  };

  const removeFromLocalStorage = (id) => {
    const products = localStorage.getItem("products")
      ? JSON.parse(localStorage.getItem("products"))
      : {};

    delete products[id];

    localStorage.setItem("products", JSON.stringify(products));
    getCartProducts();
  };

  useEffect(() => {
    calculateTotalPrice();
  },[cartProducts]);

  const calculateTotalPrice = () => {
    getCartProducts();
    let prices = 0;
    for (const [key, value] of Object.entries(cartProducts)) {
      prices += calculateTheFinalPrice(
        value.price,
        value.discount,
        value.setquantity
      );
    }
    setTotalPrice(prices);
  };

  const columns = [
    { id: "image", label: "عکس محصول" },
    { id: "name", label: "عنوان محصول" },
    {
      id: "initailPrice",
      label: "قیمت واحد",
      minWidth: 170,
    },
    {
      id: "discount",
      label: "تخفیف",
    },
    {
      id: "quantity",
      label: "تعداد",
    },
    {
      id: "finalPrice",
      label: "مجموع",
    },
    {
      id: "remove",
      label: "حذف",
    },
  ];

  const tableBodyRender = [];

  for (const [key, value] of Object.entries(cartProducts)) {
    tableBodyRender.push(
      <TableRowBuilder
        key={value._id}
        product={value}
        setquantity={value.setquantity ? value.setquantity : 1}
        removeHandler={removeFromLocalStorage}
        totalPriceHandler={calculateTotalPrice}
      />
    );
  }

  return (
    <Paper className={classes.root} elevation={3}>
      <TableContainer className={classes.container}>
        <Table className={classes.table} aria-label="cart table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell key={column.id} align="right">
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>{tableBodyRender}</TableBody>
        </Table>
      </TableContainer>
      <div className={classes.total_price_container}>
        <Typography>مجموع: {format(totalPrice)} تومان</Typography>
      </div>
    </Paper>
  );
};

export default CartTable;
