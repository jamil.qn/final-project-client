import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { format, calculateTheFinalPrice } from "../../utils";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "space-around",
  },
  discountArea: {
    display: "flex",
  },
  discountBox: {
    width: "40px !important",
    height: "30px !important",
    color: '#fff',
    fontWeight: "bold",
    fontSize: "14px",
    backgroundColor: theme.palette.primary.main,
    borderRadius: "20px !important",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "10px",
  },
  initalPrice: {
    color: "gray !important",
  },
}));

const PriceContainer = ({ price, discount, quantity }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.discountArea}>
        {discount > 0 ? (
          <>
            <div className={classes.discountBox}>{`${format(discount)}%`}</div>
            <Typography
              variant="subtitle1"
              color="inherit"
              className={classes.initalPrice}
            >
              <s>{format(price)}</s>
            </Typography>
          </>
        ) : null}
      </div>
      <Typography variant="subtitle1">
        {" "}
        {`${format(calculateTheFinalPrice(price, discount, quantity))} تومان`}
      </Typography>
    </div>
  );
};

export default PriceContainer;
