import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
  },
  addtoCartBtn: {
    "& .MuiButton-endIcon": {
      marginRight: theme.spacing(2),
    },
    margin: theme.spacing(2, 0),
  },
}));

const AddToCartButton = ({onClick}) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Button
        color="primary"
        size="large"
        className={classes.addtoCartBtn}
        endIcon={<AddShoppingCartIcon />}
        onClick={onClick}
      >
        افزودن به سبد خرید
      </Button>
    </div>
  );
};

export default AddToCartButton;
