import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { format } from "../../../utils";

const useStyles = makeStyles({
  quantity: {
    display: "flex",
    flexDirection: "row-reverse",
  },
  input_control_quantity: {
    border: "1px solid rgb(202, 202, 202) !important",
    borderRight: "none",
    borderLeft: "none",
    borderRadius: "none !important",
    width: "45px",
    textAlign: "center !important",
  },
});

const QuantityCounter = ({ inventory, quantity, quantityHandler }) => {
  const classes = useStyles();

  const increment = () => quantityHandler(quantity + 1);

  const decrement = () => quantityHandler(quantity - 1);

  return (
    <div className={classes.quantity} dir="ltr">
      <Button
      style={{minWidth: 40, borderTopLeftRadius: 'inherit', borderBottomLeftRadius: 'inherit'}}
        variant="contained"
        color="primary"
        onClick={decrement}
        disabled={quantity > 1 ? false : true}
      >
        -
      </Button>
      <input
        value={format(quantity)}
        className={classes.input_control_quantity}
        readOnly
      />
      <Button
      style={{minWidth: 40, borderTopRightRadius: 'inherit', borderBottomRightRadius: 'inherit'}}
        variant="contained"
        color="primary"
        onClick={increment}
        disabled={quantity < inventory ? false : true}
      >
        +
      </Button>
    </div>
  );
};

export default QuantityCounter;
