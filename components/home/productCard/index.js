import Link from "next/link";
import { useRouter } from "next/dist/client/router";
import { Card, CardContent, CardMedia, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import PriceContainer from "../../global/PriceContainer";
import AddToCartButton from "../../global/buttons/AddToCartButton";
import {addtoCart} from "../../../utils";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: "none",
    "&:hover": {
      transform: "scale(1.02)",
      boxShadow: "0 4px 8px 0 rgb(0 0 0 / 8%)",
      border: "1px solid #e6e6e6",
    },
  },
  notInventory: {
    textAlign: "center",
    margin: "16px 0",
    padding: "8px 0",
    color: theme.palette.primary.main,
  },
}));

const ProductCard = ({ product }) => {
  const router = useRouter();
  const classes = useStyles();

  const addToCartHandler = () => {
    addtoCart(product, 1);
    router.push("/cart");
  }

  return (
    <Card className={classes.root}>
      <Link href={`/product-detail/${product._id}`}>
        <a>
          <CardMedia
            component="img"
            alt={product.pictures[0].alt}
            image={product.pictures[0].path}
            title={product.pictures[0].title}
          />
          <CardContent>
            <Typography gutterBottom variant="body2" component="h2">
              {product.name}
            </Typography>
          </CardContent>
          <PriceContainer
            price={product.price}
            discount={product.discount}
            quantity={1}
          />
        </a>
      </Link>
      {product.inventory > 0 ? (
        <AddToCartButton onClick={addToCartHandler} />
      ) : (
        <div className={classes.notInventory}>
          <Typography gutterBottom variant="body1" component="h2">
            ناموجود
          </Typography>
        </div>
      )}
    </Card>
  );
};

export default ProductCard;
