import { Typography, useMediaQuery } from "@material-ui/core";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { Swiper, SwiperSlide } from "swiper/react";

import ProductCard from "../productCard";

import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";
import SwiperCore, { Autoplay, Navigation } from "swiper/core";
SwiperCore.use([Autoplay, Navigation]);

const useStyles = makeStyles((theme) => ({
  headerSlider: {
    paddingBottom: theme.spacing(2),
    marginBottom: theme.spacing(2),
    maxWidth: "max-content",
    borderBottom: `2px solid ${theme.palette.primary.main}`,
  },
  swiper_container: {
    width: "100% !important",
    height: "auto",
    padding: theme.spacing(1),
  },
  swiper_slide: {
    textAlign: "start",
    "& img": {
      height: "300px",
    },
  },
}));

const ProductsSlideShow = ({ products }) => {
  const classes = useStyles();
  const theme = useTheme();
  const upOfMd = useMediaQuery(theme.breakpoints.only("lg"));
  const inMd = useMediaQuery(theme.breakpoints.only("md"));
  const inXs = useMediaQuery(theme.breakpoints.only("xs"));
  const inSm = useMediaQuery(theme.breakpoints.only("sm"));

  const productCardsRender = products.slice(0, 20).map(({ product }) => {
    return (
      <SwiperSlide key={product._id} className={classes.swiper_slide}>
        <ProductCard product={product} />
      </SwiperSlide>
    );
  });

  return (
    <>
      <div className={classes.headerSlider}>
        <Typography variant="h5">جدیدترین محصولات</Typography>
      </div>
      <Swiper
        style={{ "--swiper-navigation-color": theme.palette.primary.main }}
        spaceBetween={10}
        slidesPerView={
          (upOfMd && 4) || (inMd && 3) || (inSm && 2) || (inXs && 1)
        }
        autoplay={{
          delay: 5000,
          disableOnInteraction: false,
        }}
        navigation={true}
        className={classes.swiper_container}
      >
        {productCardsRender}
      </Swiper>
    </>
  );
};

export default ProductsSlideShow;
