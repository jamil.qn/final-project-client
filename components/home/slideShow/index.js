import Image from "next/image";
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";
import "swiper/components/navigation/navigation.min.css";

import classes from "./SlideShow.module.css";

// import Swiper core and required modules
import SwiperCore, { Autoplay, Pagination, Navigation } from "swiper/core";

// install Swiper modules
SwiperCore.use([Autoplay, Pagination, Navigation]);

const SlideShow = () => {
  const images = [
    "https://i.pinimg.com/originals/02/a0/5c/02a05cb4a3e6dd1ed8381ecf8eefe966.jpg",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/1996_Citro%C3%ABn_Xantia_VSX_hatchback_%282015-11-11%29_01.jpg/1200px-1996_Citro%C3%ABn_Xantia_VSX_hatchback_%282015-11-11%29_01.jpg",
    "https://media.autoweek.nl/m/m1by0d5ben7v.jpg",
    "https://images.honestjohn.co.uk/imagecache/file/width/640/media/7031975/Citroen%20Xantia%20(2).jpg",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/1996_Citro%C3%ABn_Xantia_VSX_hatchback_%282015-11-11%29_01.jpg/1200px-1996_Citro%C3%ABn_Xantia_VSX_hatchback_%282015-11-11%29_01.jpg",
    "https://media.autoweek.nl/m/m1fyhdqb04nd_480.jpg",
    "https://amklassiek.nl/wp-content/uploads/2019/07/Xantia-1.jpg",
  ];

  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("sm"));

  const SwiperSlideRender = images.map((image, index) => (
    <SwiperSlide key={index} className={classes.swiper_slide}>
      <Image src={image} alt="zantia" layout="fill" />
    </SwiperSlide>
  ));

  return (
    <>
      <Swiper
        spaceBetween={30}
        centeredSlides={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        className={classes.swiper_container}
        style={{ height: `${matches ? "550px" : "250px"}`, '--swiper-navigation-color': theme.palette.primary.main}}
      >
        {SwiperSlideRender}
      </Swiper>
    </>
  );
};

export default SlideShow;
