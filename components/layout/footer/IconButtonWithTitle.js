import {Grid,Typography,Button,} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  buttonText: {
    display: 'flex',
    flexDirection: 'column',
    textTransform: 'lowercase',
    fontSize: '10px',
    [theme.breakpoints.up("sm")]: {
      fontSize: '16px',
    },
    '& .MuiButton-startIcon': {
      marginLeft: theme.spacing(1),
    },
}
}));


const IconButtonWithTitle = ({title, icon, buttonInfo}) =>{
  const classes = useStyles();

    return (
        <Grid item xs={6}>
          <Typography variant="subtitle2">
           {`${title}:`}
            <Button startIcon={icon} className={classes.buttonText}>
              {buttonInfo}
            </Button>
          </Typography>
        </Grid>
    );
}

export default IconButtonWithTitle;