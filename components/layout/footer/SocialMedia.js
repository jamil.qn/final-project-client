import { Grid, Typography, IconButton } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import TelegramIcon from "@material-ui/icons/Telegram";
import InstagramIcon from "@material-ui/icons/Instagram";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import WhatsAppIcon from "@material-ui/icons/WhatsApp";

const useStyles = makeStyles((theme) => ({
  socialMedia: {
    textAlign: "center",
    marginTop: theme.spacing(2)
  },
}));

const SocialMedia = () => {
  const classes = useStyles();
  return (
    <Grid item xs={12}>
      <div className={classes.socialMedia}>
        <Typography variant="subtitle1">با ما همراه شوید</Typography>
        <IconButton aria-label="tegram">
          <TelegramIcon fontSize="large" />
        </IconButton>
        <IconButton aria-label="instagram">
          <InstagramIcon fontSize="large" />
        </IconButton>
        <IconButton aria-label="linkedIn">
          <LinkedInIcon fontSize="large" />
        </IconButton>
        <IconButton aria-label="whatsApp">
          <WhatsAppIcon fontSize="large" />
        </IconButton>
      </div>
    </Grid>
  );
};

export default SocialMedia;
