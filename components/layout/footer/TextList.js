import { ListSubheader, List, ListItem, ListItemText } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  listSubHeader: {
    color: "rgba(0, 0, 0, 0.973) !important",
    fontSize: "large !important",
    fontWeight: "bold !important",
  },
  listItem: {
    textAlign: "right !important",
    color: "rgba(0, 0, 0, 0.54) !important",
    width: 'max-content',
  },
}));

const TextList = ({ subHeader, listItem }) => {
  const classes = useStyles();

  const renderListItem = listItem.map((item) => {
    return (
      <ListItem key={item} className={classes.listItem} button>
        <ListItemText primary={item} />
      </ListItem>
    );
  });

  return (
    <List
      component="ul"
      subheader={
        <ListSubheader
          className={classes.listSubHeader}
          component="div"
          id="nested-list-subheader"
        >
          {subHeader}
        </ListSubheader>
      }
    >
      {renderListItem}
    </List>
  );
};

export default TextList;
