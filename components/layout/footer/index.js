import { Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import PhoneRoundedIcon from "@material-ui/icons/PhoneRounded";
import MailOutlineRoundedIcon from "@material-ui/icons/MailOutlineRounded";

import TextList from "./TextList";
import IconButtonWithTitle from "./IconButtonWithTitle";
import SocialMedia from "./SocialMedia";

const useStyles = makeStyles((theme) => ({
  footer: {
    backgroundColor: "rgba(240, 240, 240, 0.884)",
    padding: 20,
  },
  contactInfoContainer: {
    margin: theme.spacing(3, 0),
  },
  storeAddressBox: {
    margin: theme.spacing(4, 0),
  },
}));

const Footer = () => {
  const classes = useStyles();
  return (
    <div className={classes.footer}>
      <Grid container>
        <Grid item xs={6} md={3}>
          <TextList
            subHeader="دسته بندی ها"
            listItem={[
              "لوازم موتور",
              " لوازم گیربکس",
              "لوازم شاسی و بدنه",
              "لوازم برقی",
              "لوازم تعلیق و فرمان",
            ]}
          />
        </Grid>
        <Grid item xs={6} md={3}>
          <TextList
            subHeader="خدمات مشتریان"
            listItem={[
              " موتورلوازم",
              " لوازم گیربکس",
              "لوازم شاسی و بدنه",
              "لوازم برقی",
              "لوازم تعلیق و فرمان",
            ]}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Grid container className={classes.contactInfoContainer}>
            <Typography variant="h6">
              هفت روز هفته، هر ساعتی از شبانه روز همراه شما هستیم
            </Typography>
            <Typography variant="subtitle1" className={classes.storeAddressBox}>
              آدرس فروشگاه:
              <span>
                مازندران، ساری،بلوار دانشجو،نبش هنرستان تربیت بدنی داراب
              </span>
            </Typography>
            <IconButtonWithTitle
              title=" شماره تماس"
              icon={<PhoneRoundedIcon />}
              buttonInfo="۰۱۱-۳۳۳۲۱۹۱۳ / ۰۹۱۱۱۵۲۱۰۱۲"
            />
            <IconButtonWithTitle
              title=" آدرس ایمیل"
              icon={<MailOutlineRoundedIcon />}
              buttonInfo="zantia.yadak@gmail.com"
            />
            <SocialMedia />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h5">چرا زانیا یدک؟</Typography>
          <br />
          <Typography variant="body1">
            زانتیا یدک یکی از بزرگترین و معتبرترین فروشگاه لوازم یدکی زانیا در
            ایران میباشد که به لطف اطمینان و حمایت شما عزیزان توانسته است گامی
            بزرگ در تامین و توزیع لوازم زانتیا داشته باشد. بزرگترین شعار مجموعه
            زانتیا یدک، سرعت در تامین و توزیع بمنظور رضایت تام مشتریان عزیز در
            هر نقطه از کشور عزیزمان ایران میباشد و همین شعار سبب گردیده که هر
            روز خود رو با نیاز های مشتری همسو کنیم و در این مسیر خود را ارتقا
            ببخشیم.
          </Typography>
        </Grid>
      </Grid>
    </div>
  );
};

export default Footer;
