import { Container } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Navbar from "./navbar";
import Footer from "./footer";

const useStyles = makeStyles((theme) => ({
  main: {
    margin: theme.spacing(17, 0, 5, 0),
  },
}));

const Layout = ({ children, categories }) => {
  const classes = useStyles();

  return (
    <div>
      <header>
        <Navbar categories={categories} />
      </header>
      <main className={classes.main}>
        <Container>{children}</Container>
      </main>
      <footer>
        <Footer />
      </footer>
    </div>
  );
};

export default Layout;
