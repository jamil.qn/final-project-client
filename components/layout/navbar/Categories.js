import { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Divider, Tab } from "@material-ui/core";
import { TabContext, TabList, TabPanel } from "@material-ui/lab";
import ChevronLeftRoundedIcon from "@material-ui/icons/ChevronLeftRounded";
import CategorySubList from "./CategorySubList";
import SeaAllBtn from "./SeaAllBtn";

const useStyles = makeStyles((theme) => ({
  root: {
    position: "absolute",
    right: 16,
    paddingTop: 0,
    zIndex: 1200,
    display: "none",
  },
  megaMenuContainer: {
    display: "flex",
    flexGrow: 1,
    maxWidth: "max-content",
    maxHeight: "max-content",
    color: "#000000",
    boxShadow: "0px 1px 15px -1px rgba(33, 33, 33, 0.4)",
    borderRadius: " 16px 0px 16px 16px",
    background:
      "linear-gradient(0deg, rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 0.5)), linear-gradient(119.88deg, rgba(255, 255, 255, 0.4) 2.22%, rgba(255, 255, 255, 0.1) 117.11%)",
    backdropFilter: "blur(30px)",
  },
  tab: {
    minHeight: "48px",
    minWidth: "max-content",
    padding: theme.spacing(0, 3, 0, 2),
    fontSize: "1rem",
    letterSpacing: "normal",
    textTransform: "capitalize",
    opacity: 1,
    fontWeight: "normal",
    "& .MuiTab-wrapper": {
      flexDirection: "row-reverse",
      justifyContent: "space-between",
      "& .MuiSvgIcon-root": {
        marginBottom: 0,
        transform: "translate(-6px)",
      },
    },
  },
  tabListContainer: {
    display: "flex",
    flexDirection: "column",
  },
  tabPanel: {
    padding: 0,
    display: "flex",
  },
}));

const Categories = ({ show, categories }) => {
  const classes = useStyles();
  const [value, setValue] = useState("1");
  const [open, setOpen] = useState(false);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const renderTab = categories
    .slice(0, 4)
    .map(({category}, index) => (
      <Tab
        key={category.maincategory._id}
        label={category.maincategory.name}
        value={(index + 1).toString()}
        icon={<ChevronLeftRoundedIcon />}
        className={classes.tab}
      />
    ));

  const renderTabPanel = categories.slice(0, 4).map(({category}, index) => (
    <TabPanel
      key={index}
      value={(index + 1).toString()}
      className={classes.tabPanel}
    >
      <CategorySubList subCategories={category.subCategories} />
    </TabPanel>
  ));

  return (
    <div
      className={classes.root}
      data-testid="destination-test"
      onMouseOver={() => setOpen(true)}
      onFocus={() => setOpen(true)}
      onMouseLeave={() => setOpen(false)}
      style={show || open ? { display: "flex" } : { display: "none" }}
    >
      <div className={classes.megaMenuContainer}>
        <TabContext value={value}>
          <div className={classes.tabListContainer}>
            <TabList onChange={handleChange} orientation="vertical">
              {renderTab}
            </TabList>
            <SeaAllBtn href="/" />
          </div>
          <Divider orientation="vertical" />
          {renderTabPanel}
        </TabContext>
      </div>
    </div>
  );
};

export default Categories;
