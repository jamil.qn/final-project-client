import Link from "next/link";
import { makeStyles } from "@material-ui/core/styles";
import { List, ListItem, ListItemText } from "@material-ui/core";
import SeaAllBtn from "./SeaAllBtn";

const useStyles = makeStyles((theme) => ({
  root: {},
  list: {
    display: "flex",
    flexDirection: "column",
    minWidth: "150px",
    maxHeight: 200,
    flexWrap: "wrap",
    padding: theme.spacing(0),
  },
}));

const CategorySubList = ({ subCategories }) => {
  const classes = useStyles();

  const renderListItem = subCategories.slice(0, 8).map(( subCategory ) => (
    <ListItem key={subCategory._id}>
      <Link href="/">
        <a>
          <ListItemText primary={subCategory.name} />
        </a>
      </Link>
    </ListItem>
  ));

  return (
    <div className={classes.root}>
      <div style={{ display: "flex" }}>
        <List className={classes.list}>{renderListItem.slice(0, 4)}</List>
        <List className={classes.list}>{renderListItem.slice(4, 8)}</List>
      </div>
      {subCategories.length > 8 ? <SeaAllBtn href="/" /> : null}
    </div>
  );
};

export default CategorySubList;
