import Link from 'next/link';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    margin: '2px 16px 22px',
    textAlign: 'start',
  },
  seeAllBtn: {
    fontSize: '0.75rem',
    textTransform: 'capitalize',
    textDecoration: 'underline',
  },
});

const SeaAllBtn = ({ href }) => {
  const classes = useStyles();
  return (
    <div className={classes.root} data-testid="sea-all-button">
      <Link href={href}>
        <a className={classes.seeAllBtn}>
          موارد بیشتر
        </a>
      </Link>
    </div>
  );
};

export default SeaAllBtn;
