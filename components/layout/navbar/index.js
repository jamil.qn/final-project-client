import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/dist/client/router";
import { makeStyles } from "@material-ui/core/styles";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  InputBase,
  Badge,
  MenuItem,
  Menu,
  Button,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MoreIcon from "@material-ui/icons/MoreVert";
import ShoppingCartRoundedIcon from "@material-ui/icons/ShoppingCartRounded";
import MenuRoundedIcon from "@material-ui/icons/MenuRounded";
import Categories from "./Categories";

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  appBar: {
    color: "black",
    boxShadow: "0px 0.888889px 21.3333px -0.888889px rgba(255, 255, 255, 0.45)",
    background:
      "linear-gradient(0deg, rgba(255, 255, 255, 0.8), rgba(255, 255, 255, 0.8)), linear-gradient(120.18deg, rgba(255, 255, 255, 0.4) 0%, rgba(255, 255, 255, 0.1) 98.54%)",
    "&::before": {
      content: '""',
      position: "absolute",
      zIndex: "-1",
      backdropFilter: "blur(17.7778px)",
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    },
  },
  toolbar: {
    minHeight: 80,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: "block",
    overflow: "visible",
    fontFamily: "'Nosifer', cursive",
    [theme.breakpoints.only('xs')]: {
      fontSize: '12px',
    }
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    background: "#f0f0f1",
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(2, 7, 2, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(3)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  categories: {
    position: "relative",
    width: "max-content",
  },
  categoriesBtn: {
    maxWidth: "max-content",
    margin: theme.spacing(0, 2, 0, 0),
    "& .MuiButton-startIcon": {
      marginLeft: theme.spacing(2),
    },
  },
}));

const NavBar = ({categories}) => {
  
  const router = useRouter();
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const [openMegaMenu, setOpenMegaMenu] = useState(false);
  const [quantityInLocaleStoage, setQuantityInLocaleStoage] = useState(0);
  useEffect(() => {
    setQuantityInLocaleStoage(localStorage.getItem("products") ? Object.keys(JSON.parse(localStorage.getItem("products"))).length : 0);
  });

  const handleMegaMenuClose = () => {
    setTimeout(() => {
      setOpenMegaMenu(false);
    }, 50);
  };

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
    </Menu>
  );

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem onClick={() => router.push("/cart")}>
        <IconButton aria-label="show 4 new mails" color="inherit">
          <Badge badgeContent={quantityInLocaleStoage} color="primary">
            <ShoppingCartRoundedIcon />
          </Badge>
        </IconButton>
        <p>سبد خرید</p>
      </MenuItem>
      <MenuItem onClick={() => router.push("/admin/signin")}>
        <IconButton aria-label="account of current user" color="inherit">
          <AccountCircle />
        </IconButton>
        <p>ورود به پروفایل</p>
      </MenuItem>
    </Menu>
  );

  return (
    <div className={classes.grow}>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <Link href="/">
            <a>
              <Typography className={classes.title} variant="h6" noWrap>
                Xantia Yadak
              </Typography>
            </a>
          </Link>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="جستوجو در زانتیا یدک..."
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ "aria-label": "search" }}
            />
          </div>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <IconButton
              aria-label="show 4 new mails"
              color="inherit"
              onClick={() => router.push("/cart")}
            >
              <Badge badgeContent={quantityInLocaleStoage} color="primary">
                <ShoppingCartRoundedIcon />
              </Badge>
            </IconButton>
            <IconButton
              onClick={() => router.push("/admin/signin")}
              aria-label="account of current user"
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
        <div
          className={classes.categories}
          onMouseOver={() => setOpenMegaMenu(true)}
          onFocus={() => setOpenMegaMenu(true)}
          onMouseLeave={handleMegaMenuClose}
        >
          <Button
            className={classes.categoriesBtn}
            startIcon={<MenuRoundedIcon />}
          >
            دسته بندی
          </Button>
          <Categories show={openMegaMenu} categories={categories} />
        </div>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </div>
  );
};

export default NavBar;
