import { useState } from "react";
import { useRouter } from "next/dist/client/router";
import { Divider, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import QuantityCounter from "../../global/buttons/QuantityCounter";
import PriceContainer from "../../global/PriceContainer";
import AddToCartButton from "../../global/buttons/AddToCartButton";
import {addtoCart} from "../../../utils";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: "#f5f5f5",
    borderRadius: "10px",
    padding: "10px",
  },
  section1: {
    display: "flex",
    justifyContent: "space-between",
    margin: "30px 20px",
  },
  section2: {
    margin: "20px",
  },
  section3: {
    margin: "10px",
  },
  notInventory: {
    textAlign: "center",
    margin: "16px 0",
    padding: "8px 0",
    color: theme.palette.primary.main,
  },
}));

const OrderCard = ({ product }) => {
  const router = useRouter();
  const classes = useStyles();

  const [quantity, setQuantity] = useState(product.setquantity ? product.setquantity : (product.inventory > 0 ? 1 : 0));

  const quantityHandler = (count) => {
    setQuantity(count);
  };

  const addToCartHandler = () => {
    addtoCart(product, quantity);
    router.push("/cart");
  }

  return (
    <div className={classes.root}>
      <div className={classes.section1}>
        <Typography color="textSecondary" variant="subtitle1">
          تعداد:
        </Typography>
        <QuantityCounter
          inventory={product.inventory}
          quantity={quantity}
          quantityHandler={quantityHandler}
        />
      </div>
      <Divider variant="middle" />
      <div className={classes.section2}>
        <PriceContainer
          price={product.price}
          discount={product.discount}
          quantity={quantity}
        />
      </div>
      <Divider variant="middle" />
      <div className={classes.section3}>
        {product.inventory > 0 ? (
          <AddToCartButton onClick={addToCartHandler} />
        ) : (
          <div className={classes.notInventory}>
            <Typography gutterBottom variant="body1" component="h2">
              ناموجود
            </Typography>
          </div>
        )}
      </div>
    </div>
  );
};

export default OrderCard;
