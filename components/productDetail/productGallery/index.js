import React, { useRef, useState } from "react";
import { useTheme } from "@material-ui/core/styles";
import Image from "next/image";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/swiper.min.css";
import "swiper/components/navigation/navigation.min.css";
import "swiper/components/thumbs/thumbs.min.css";

// import Swiper core and required modules
import SwiperCore, { Navigation, Thumbs } from "swiper/core";

// install Swiper modules
SwiperCore.use([Navigation, Thumbs]);

const ProductGallery = ({ pictures }) => {
  const theme = useTheme();
  const [thumbsSwiper, setThumbsSwiper] = useState(null);

  const galleryImagesRender = pictures.map((picture) => {
    return (
      <SwiperSlide key={picture._id}>
        <div className="swiper-slide-image-box">
          <Image
            src={picture.path}
            alt={picture.alt}
            layout="responsive"
            width="100%"
            height="100%"
          />
        </div>
      </SwiperSlide>
    );
  });

  return (
    <>
      <Swiper
      style={{ "--swiper-navigation-color": theme.palette.primary.main }}
        spaceBetween={10}
        navigation={true}
        thumbs={{ swiper: thumbsSwiper }}
        className="mySwiper2"
      >
        {galleryImagesRender}
      </Swiper>
      <Swiper
        onSwiper={setThumbsSwiper}
        spaceBetween={10}
        slidesPerView={4}
        freeMode={true}
        watchSlidesVisibility={true}
        watchSlidesProgress={true}
        className="mySwiper"
      >
        {galleryImagesRender}
      </Swiper>
    </>
  );
};

export default ProductGallery;
