import Link from "next/link";
import { Breadcrumbs } from "@material-ui/core";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";

const ProductNav = ({ mainCategory, subCategory, productName }) => {
  return (
    <div id="bread-crumb">
      <Breadcrumbs
        separator={<NavigateBeforeIcon fontSize="small" />}
        aria-label="breadcrumb"
      >
        <Link color="inherit" href="/">
          <a>زانتیا یدک</a>
        </Link>
        <Link color="inherit" href="/">
          <a>{mainCategory}</a>
        </Link>
        <Link color="inherit" href="/">
          <a>{subCategory}</a>
        </Link>
        <span style={{ color: "#a5a5a5" }}>{productName}</span>
      </Breadcrumbs>
    </div>
  );
};

export default ProductNav;
