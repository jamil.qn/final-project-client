import Image from "next/image";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  service_boxs_container: {
    [theme.breakpoints.up("md")]: {
      padding: "40px 20px",
      borderTop: "1px solid rgba(0, 0, 0, 0.12)",
      borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
    },
    [theme.breakpoints.between("xs", "sm")]: {
      display: "flex",
      justifyContent: "space-around",
      width: "100%",
      flexWrap: "wrap",
    },
  },
  service_box: {
    marginBottom: "40px",
    display: "flex",
    listStyleType: "none",
  },
  service_image_box: {
    width: 30,
    height: 30,
    marginLeft: 4,
  },
}));

const ProductService = () => {
  const classes = useStyles();
  return (
    <div className={classes.service_boxs_container}>
      <li className={classes.service_box}>
        <div className={classes.service_image_box}>
          <Image
            src="/assets/images/quaranty.png"
            alt="ضمانت کالا"
            layout="responsive"
            width={30}
            height={30}
          />
        </div>
        <span>ضمانت کالا</span>
      </li>
      <li className={classes.service_box}>
        <div className={classes.service_image_box}>
          <Image
            src="/assets/images/delivery_fast.png"
            alt="ارسال سریع"
            layout="responsive"
            width={30}
            height={30}
          />
        </div>
        <span>ارسال سریع</span>
      </li>

      <li className={classes.service_box}>
        <div className={classes.service_image_box}>
          <Image
            src="/assets/images/hand_card.png"
            alt="پرداخت درب منزل"
            layout="responsive"
            width={30}
            height={30}
          />
        </div>
        <span>پرداخت درب منزل</span>
      </li>
      <li className={classes.service_box}>
        <div className={classes.service_image_box}>
          <Image
            src="/assets/images/return_cart.png"
            alt="بازگشت کالا تا یک هفته"
            layout="responsive"
            width={30}
            height={30}
          />
        </div>
        <span>بازگشت کالا تا یک هفته</span>
      </li>
    </div>
  );
};

export default ProductService;
