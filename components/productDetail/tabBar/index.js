import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { AppBar, Tab, Typography } from "@material-ui/core";
import { TabContext, TabList, TabPanel } from "@material-ui/lab";

const useStyles = makeStyles((theme) => ({
  appBar: {
    background: "#fff",
    boxShadow: "none",
    color: "#000",
    borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
  },
  tabIndicator: {
    background: theme.palette.primary.main,
  },
}));

const Tabs = ({ description, specification }) => {
  const classes = useStyles();
  const [value, setValue] = useState("1");
 
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div>
      <TabContext value={value}>
        <AppBar position="static" className={classes.appBar}>
          <TabList
            onChange={handleChange}
            TabIndicatorProps={{ className: classes.tabIndicator }}
          >
            <Tab label="توضیحات" value="1" />
            <Tab label="ویژگی ها" value="2" />
          </TabList>
        </AppBar>
        <TabPanel value="1">
          <div dangerouslySetInnerHTML={{__html: description}}></div>
        </TabPanel>
        <TabPanel value="2">
          <div  dangerouslySetInnerHTML={{__html: specification}}></div>
        </TabPanel>
      </TabContext>
    </div>
  );
};

export default Tabs;
