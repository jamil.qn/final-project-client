module.exports = {
  reactStrictMode: true,
  images: {
    domains: [
      "res.cloudinary.com",
      "media.autoweek.nl",
      "upload.wikimedia.org",
      "logosvector.net",
      "images.honestjohn.co.uk",
      "amklassiek.nl",
      "i.pinimg.com",
    ],
  },
};
