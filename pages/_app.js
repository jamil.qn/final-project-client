import Head from "next/head";
import { ThemeProvider, createTheme } from "@material-ui/core/styles";
import "../styles/globals.css";
import "../styles/productGallery.css";
import 'react-quill/dist/quill.snow.css';

const theme = createTheme({
  overrides: {
    MuiTab: {
      root: {
        "&$selected": {
          boxShadow:
            "0.888889px 0.888889px 21.3333px -0.888889px rgba(0, 0, 0, 0.1)",
          fontWeight: 600,
          position: "relative",
          "&::before": {
            content: '""',
            position: "absolute",
            zIndex: "-1",
            filter: "blur(45px)",
            background:
              "linear-gradient(0deg, rgba(33, 33, 33, 0.63), rgba(33, 33, 33, 0.63))",
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
          },
          backgroundBlendMode: "overlay",
          "& .MuiTab-wrapper": {
            "& .MuiSvgIcon-root": {
              transform: "translate(-12px)",
            },
          },
        },
      },
    },
    MuiStepConnector: {
      alternativeLabel: {
        left: 'calc(50% + 20px)',
        right:'calc(-50% + 20px)',
      },
    },
  },
  typography: {
    fontFamily: `'Vazir', sans-serif`,
    fontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
  },
  palette: {
    primary: {
      main: '#2C5364',
    }
  }
});

function MyApp({ Component, pageProps }) {
  return (
    <ThemeProvider theme={theme}>
      <Head>
        <link
          href="https://cdn.jsdelivr.net/gh/rastikerdar/vazir-font@v30.0.0/dist/font-face.css"
          rel="stylesheet"
          type="text/css"
        />
      </Head>
      <Component {...pageProps} />
    </ThemeProvider>
  );
}

export default MyApp;
