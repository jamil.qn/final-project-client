import Link from "next/link";
import Head from "next/head";
import { Button, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import AuthProvider from "../../../../components/AuthProvider";
import Layout from "../../../../components/admin/Layout";
import EditForm from "../../../../components/admin/product-categories/edit/EditForm";
import xantiayadakAPI from "../../../../apis/xantiayadak";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    padding: "20px",
  },
  addBtn: {
    "& .MuiButton-startIcon": {
      marginLeft: "10px",
    },
  },
  title: {
    textAlign: "center",
    margin: "20px",
    fontSize: "32px",
    textShadow: `1px 1px 5px ${theme.palette.primary.main}`,
  },
}));

export default function EditCategory({ productCategories, category }) {
  const classes = useStyles();

  return (
    <>
      <Head>
        <title>ویرایش دسته {category.name} | پنل ادمین زانتیا یدک</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/xantia-logo-svg-vector.svg" />
      </Head>
      <Layout>
        <AuthProvider>
          <Paper className={classes.root}>
            <Link href="/admin/product-categories/product-categories-list">
              <a>
                <Button
                  className={classes.addBtn}
                  startIcon={<ChevronRightIcon />}
                >
                  بازگشت به لیست دسته بندی ها
                </Button>
              </a>
            </Link>
            <Typography varinat="h1" className={classes.title}>
              ویرایش دسته
            </Typography>
            <EditForm data={category} />
          </Paper>
        </AuthProvider>
      </Layout>
    </>
  );
}

export const getStaticPaths = async () => {
  const categories = await xantiayadakAPI.get("/product-categories");

  return {
    fallback: "blocking",
    paths: categories.data.data.map((category) => ({
      params: { categoryId: category._id },
    })),
  };
};

export const getStaticProps = async (context) => {
  const categoryId = context.params.categoryId;

  const category = await xantiayadakAPI.get(
    `/product-categories/${categoryId}`
  );

  return {
    props: {
      category: category.data.data,
    },
    revalidate: 1 * 5,
  };
};
