import { Fragment } from "react";
import Head from "next/head";
import xantiayadakAPI from "../apis/xantiayadak";
import SlideShow from "../components/home/slideShow";
import ProductsSlideShow from "../components/home/productsSlideShow";
import Layout from "../components/layout";

export default function Home(props) {
  return (
    <div>
      <Head>
        <title>فروشگاه اینترنتی زانتیا یدک</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/xantia-logo-svg-vector.svg" />
      </Head>
      <Layout categories={props.categories}>
        <Fragment>
          <SlideShow />
          <ProductsSlideShow products={props.products} />
        </Fragment>
      </Layout>
    </div>
  );
}

export const getStaticProps = async () => {
  const products = await xantiayadakAPI.get("/products");
  const megaMenuCategories = await xantiayadakAPI.get(
    "/product-categories/navbar-menu"
  );
  return {
    props: {
      products: products.data.data.map((product) => ({
        product,
      })),
      categories: megaMenuCategories.data.data.map((category) => ({
        category,
      })),
    },
    revalidate: 1 * 5,
  };
};
