import Head from "next/head";
import xantiayadakAPI from "../../../apis/xantiayadak";
import { Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ProductNav from "../../../components/productDetail/productNav";
import ProductGallery from "../../../components/productDetail/productGallery";
import OrderCard from "../../../components/productDetail/orderCard";
import ProductService from "../../../components/productDetail/productService";
import TabBar from "../../../components/productDetail/tabBar";
import Layout from "../../../components/layout";

const useStyles = makeStyles({
  product_info_container: {
    margin: "30px 0",
  },
});

const ProductDetail = ({ product, categories }) => {
  const classes = useStyles();

  return (
    <>
      <Head>
        <title>{product.name} | فروشگاه اینترنتی زانتیا یدک</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/xantia-logo-svg-vector.svg" />
      </Head>
      <Layout categories={categories}>
        <div id="product-detail">
          <ProductNav
            mainCategory={product.product_category.mainCategory.name}
            subCategory={product.product_category.subCategory.name}
            productName={product.name}
          />
          <div className={classes.product_info_container}>
            <Grid container spacing={5}>
              <Grid item xs={12} style={{ paddingBottom: "5px" }}>
                <Typography variant="h5">{product.name}</Typography>
              </Grid>
              <Grid item xs={12} sm={6} md={4} style={{ height: "450px" }}>
                <ProductGallery pictures={product.pictures} />
              </Grid>
              <Grid item xs={12} sm={6} md={4}>
                <OrderCard product={product} />
              </Grid>
              <Grid item xs={12} md={4}>
                <ProductService />
              </Grid>
            </Grid>
          </div>
          <TabBar
            description={product.description}
            specification={product.specification}
          />
        </div>
      </Layout>
    </>
  );
};

export const getStaticPaths = async () => {
  const products = await xantiayadakAPI.get("/products");

  return {
    fallback: "blocking",
    paths: products.data.data.map((product) => ({
      params: { productId: product._id },
    })),
  };
};

export const getStaticProps = async (context) => {
  const productId = context.params.productId;

  const product = await xantiayadakAPI.get(`/products/${productId}`);
  const megaMenuCategories = await xantiayadakAPI.get(
    "/product-categories/navbar-menu"
  );
  return {
    props: {
      product: product.data.data,
      categories: megaMenuCategories.data.data.map((category) => ({
        category,
      })),
    },
    revalidate: 1 * 5,
  };
};

export default ProductDetail;
