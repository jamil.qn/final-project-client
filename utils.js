export const format = (value) => value.toLocaleString("fa");

export const calculateTheFinalPrice = (price, discount, quantity) => {
  return (price - (price * discount) / 100) * quantity;
};

export const addtoCart = (product, quantity) => {
  const products = localStorage.getItem("products")
    ? JSON.parse(localStorage.getItem("products"))
    : {};

  if (products.hasOwnProperty(product._id)) {
    products[product._id].setquantity = quantity;
  } else {
    products[product._id] = product;
    products[product._id].setquantity = quantity;
  }

  localStorage.setItem("products", JSON.stringify(products));
};
